# 2FA Banking Website
This simulated banking website was created by Ken Reese as a part of his [master's thesis](https://www.usenix.org/system/files/soups2019-reese.pdf) at Brigham Young University. 
It allows for 6 different authentication schemes which users can register and authenticate with to access a simulated banking website. 
The authentication schemes supported are:

*	SMS messages (through Twilio)
*	Push notifications (through Authy)
*	U2F
*	TOTP
*	Backup codes
*	Password only (control group)

## Setup
The site was built on Linux and may not run on other platforms. 

1. Run `npm install` inside the frontend folder 
2. Run `npm start` to start the webpack-dev-server. This is a node server that will both serve the frontend and proxy any API requests to the backend.  
3. In the backend folder, create a python virtualenv using `virtualenv env -p $(which python3)` 
4. Activate the virtualenv with `source env/bin/activate` 
5. Install dependencies with`pip install -r requirements.txt` 
6. Run a blank mongo with `docker run -t -i -p 27017:27017 mongodb`
7. Run the server with the `start_gunicorn.sh` script